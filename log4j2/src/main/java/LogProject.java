import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

class LogProject {
    private static Logger logger = LogManager.getLogger(LogProject.class);

    public static void main(String[] args) {
        logger.info("press 1 for downloading Songs");
        Scanner scan = new Scanner(System.in);
        int choice = scan.nextInt();
        logger.debug("You entered {}", choice);
        logger.info("Downloading.....");
        logger.warn("Never click on a suspicious pop-up!!! ");
        logger.trace("The TRACE Level designates finer-grained informational events than the DEBUG");
        logger.fatal("The FATAL level designates very severe error event which  lead the application to abort. ");
        try {
            divide();
        } catch (ArithmeticException e) {
            logger.error("You cannot Divide a Number with 0");
        }

    }

   public static void divide() {

        int i = 10 / 0;

    }
}